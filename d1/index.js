// SECTION - JSON Objects
/* 
    - JSON stands for JavaScript Object Notation
    - JSON is also used in other programming languages, hence the "Notation" in its name
    - JSON is not to be confused with JavaScript objects

    SYNTAX:
        {
            "propertyA" : "valueA",
            "propertyB" : "valueB",
            "propertyC" : "valueC",
        }
*/
// below is an example of an JS object
/* let cities = [
    {
        city: 'Dumaguete City',
        province: 'Negros Oriental',
        country: 'Philippines'
    },
    {
        city: 'Cebu City',
        province: 'Cebu',
        country: 'Philippines'
    },
    {
        city: 'Baguio City',
        province: 'Benguet',
        country: 'Philippines'
    }
]; */

/* 
below is an example of JSON array
    when logged in the console, it seems that the appearance of JSON does not differ from JS Object. But since JSON is used by other languages as well, it is common to see it as JS object since it is the blueprint of the JSON format
*/
/* 
    WHAT JSON DOES
    - JSON is used for serializing different data types into bytes
    - bytes is a unit of data that is composed of eight binary digits (1/0) that is used to represent a character (letters, numbers, typographical symbol)
    - serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
*/
let cities = [
    {
        'city': 'Dumaguete City',
        'province': 'Negros Oriental',
        'country': 'Philippines'
    },
    {
        'city': 'Cebu City',
        'province': 'Cebu',
        'country': 'Philippines'
    },
    {
        'city': 'Baguio City',
        'province': 'Benguet',
        'country': 'Philippines'
    }
];

console.log(cities)


// JSON Methods
    // JSON object contains methods for parsing and converting data into/from JSON or stringified JSON

// SECTION - Stringify method
/* 
    - stringified JSON is a JS object converted into a string (but in JSON format) to be used in other functions of a JS application
    - they are commonly used in HTTP request where infromation is required to be sent and received in a stringified JSON format
    - requests are an important part of programming where an applicaton communicates wth a backend application to perform different tasks such as getting/creating data in a database
*/
let batchesArr = [
    {
        batchName: "Batch X"
    },
    {
        batchName: "Batch Y"
    }
];
console.log(batchesArr);
// stringify method - used to convert JS objects into a string
console.log(`Result from stringify method:`);
console.log(JSON.stringify(batchesArr));

// mini-activity
let userData = {
    name: 'John Smith',
    age: 55,
    address: {
        city: 'New York',
        country: 'USA'
    }
}
console.log(JSON.stringify(userData));

// other code, same output from above
let data = JSON.stringify({
    name: "John",
    age: 32,
    address: {
        city: "Batangas",
        country: "Philippines"
    }
})
console.log(data);

// an example where JSON is commonly used is on package.json files which an express JS application uses to keep track of the information regarding a repository/project (see package.json)


// JSON.stringify with variables and prompt()
/* 
    - When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
    - the "property" name and the "value" name would have to be the same and this might be a little confusing, but this is to signify that the value of the variable and the property name pertains to the same thing and that is the value of the variable itself
    - this also helps in code readability
    - this is commonly used when we try to send information to the backend application
    SYNTAX:
        JSON.stringify({
            propertyA: variableA,
            propertyB: variableB
        })
*/
/* let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = Number(prompt("What is your age?"));
let address = {
    city: prompt("Which city do you live?"),
    country: prompt("Which country does your city belong?")
}

let otherData = JSON.stringify({
    fname: fname,
    lname: lname,
    age: age,
    address: address
})

console.log(otherData); */

// SECTION - parse method
/* 
    - converts the stringified JSON into JavaScript Objects
    - Objects are common data types used in applications because of the complex data structures that can be created out of them
	- information is commonly sent to applications in stringinfied JSON and then converted back into objects
	- this hanppens for both sending information to a backend application and sending information back to a frontend application

*/
let batchesJSON = `[{"batchName" : "Batch X"}, {"batchName" : "Batch Y"}]`;
console.log(batchesJSON);
console.log(`Results from parse method:`);
console.log(JSON.parse(batchesJSON));


// mini-activity
console.log(JSON.parse(data));
//console.log(JSON.parse(otherData));